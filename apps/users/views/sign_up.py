from django.contrib.auth import authenticate, login
from django.shortcuts import render, redirect
from django.views import View
from rest_framework.permissions import AllowAny

from users.form.user_creation_form import CustomUserCreationForm


class SignUpView(View):
    permission_classes = [AllowAny]
    template_name = 'signup.html'

    def get(self, request):
        context = {
            'form': CustomUserCreationForm()
        }
        return render(request, self.template_name, context)

    def post(self, request):
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=password)
            user.is_staff = True
            user.save()
            login(request, user)
            return redirect('/main/home')
        context = {
            'form': form
        }
        return render(request, self.template_name, context)

