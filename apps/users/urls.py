from django.contrib.auth.views import LoginView, LogoutView
from django.urls import path

from users.views.sign_in import SignInView
from users.views.sign_up import SignUpView

urlpatterns = [
    path('sign-in/', SignInView.as_view(), name='sign-in'),
    path('sign-up/', SignUpView.as_view(), name='sign-up'),

    path('logout/', LogoutView.as_view(), name='logout'),
]
