import json

from django.shortcuts import render
from rest_framework.permissions import AllowAny
from rest_framework.views import APIView

from main.models import Server
from main.serializers.server import ServerSerializer


class Home(APIView):
    permission_classes = (AllowAny,)

    def get(self, request):
        queryset = Server.objects.list()
        serializer = ServerSerializer(queryset).data
        return render(request, 'home.html', {'data': json.dumps(serializer)})
