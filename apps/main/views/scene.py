from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView

from main.models import Scene
from main.serializers.scene import SceneSerializer


class SceneListView(APIView):
    def put(self, request):
        instance = Scene.objects.filter(id=request.data.get('id')).first()
        serializer = SceneSerializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)


class SceneDetailView(APIView):
    def delete(self, request, pk):
        instance = get_object_or_404(Scene, pk=pk)
        instance.delete()
        return Response({}, 204)
