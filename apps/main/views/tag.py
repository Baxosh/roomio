from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView

from main.models import Tag
from main.serializers.tag import TagSerializer


class TagListView(APIView):
    def put(self, request):
        instance = Tag.objects.filter(id=request.data.get('id')).first()
        serializer = TagSerializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)


class TagDetailView(APIView):
    def delete(self, request, pk):
        instance = get_object_or_404(Tag, pk=pk)
        instance.delete()
        return Response({}, 204)
