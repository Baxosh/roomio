from rest_framework.generics import get_object_or_404
from rest_framework.permissions import AllowAny, IsAdminUser
from rest_framework.response import Response
from rest_framework.views import APIView

from main.models import ModbusServer
from main.serializers.modbus_server import ModbusServerSerializer


class ModbusServerListView(APIView):
    def put(self, request):
        instance = ModbusServer.objects.filter(id=request.data.get('id')).first()
        serializer = ModbusServerSerializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)


class ModbusServerDetailView(APIView):
    def delete(self, request, pk):
        instance = get_object_or_404(ModbusServer, pk=pk)
        instance.delete()
        return Response({}, 204)
