from rest_framework.response import Response
from rest_framework.views import APIView

from main.models import SceneTag
from main.serializers.scene_tag import SceneTagSerializer


class SceneTagListView(APIView):
    def put(self, request):
        instance = SceneTag.objects.filter(id=request.data.get('id')).first()
        serializer = SceneTagSerializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)
