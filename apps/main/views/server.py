from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView

from main.models import Server
from main.serializers.server import ServerSerializer


class ServerListView(APIView):
    def put(self, request):
        instance = Server.objects.filter(id=request.data.get('id')).first()
        serializer = ServerSerializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)


class ServerDetailView(APIView):
    def delete(self, request, pk):
        instance = get_object_or_404(Server, pk=pk)
        instance.delete()
        return Response({}, 204)


