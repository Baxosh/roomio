from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView

from main.models import Location
from main.serializers.location import LocationSerializer


class LocationListView(APIView):
    def put(self, request):
        instance = Location.objects.filter(id=request.data.get('id')).first()
        serializer = LocationSerializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)


class LocationDetailView(APIView):
    def delete(self, request, pk):
        instance = get_object_or_404(Location, pk=pk)
        instance.delete()
        return Response({}, 204)
