from django.contrib import admin

from main.models import Server, ModbusServer, Location, Tag, Scene, SceneTag


@admin.register(Server)
class ServerAdmin(admin.ModelAdmin):
    fields = (
        'name',
        'app_host',
        'app_port',
        'timeout',
        'logging',
        'check_modbus_interval',
        'check_modbus_interval_after_failed_attempt',
    )


@admin.register(ModbusServer)
class ModbusServerAdmin(admin.ModelAdmin):
    fields = (
        'device_name',
        'hrc_ip',
        'hrc_port',
        'unit_id',
        'device_template_id',
        'room',
        'device_active',
        'server',
    )


@admin.register(Location)
class LocationAdmin(admin.ModelAdmin):
    fields = ('name', 'modbus_server')


@admin.register(Scene)
class SceneAdmin(admin.ModelAdmin):
    fields = ('name', 'modbus_server')


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    fields = ('modbus_id', 'location')


@admin.register(SceneTag)
class SceneTagAdmin(admin.ModelAdmin):
    fields = ('modbus_id', 'value', 'scene')
