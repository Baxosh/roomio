from django.contrib.auth.models import User
from django.core.validators import MinLengthValidator
from django.db import models
from django.db.models import SET_NULL, CASCADE

from main.querysets.location import LocationQuerySet
from main.querysets.modbus_server import ModbusServerQuerySet
from main.querysets.scene import SceneQuerySet
from main.querysets.scene_tag import SceneTagQuerySet
from main.querysets.server import ServerQuerySet
from main.querysets.tag import TagQuerySet


class BaseModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, editable=False, null=True)
    updated_at = models.DateTimeField(auto_now=True, editable=False, null=True)
    created_by = models.ForeignKey(User, SET_NULL, null=True, blank=True, related_name='created_%(model_name)ss')
    updated_by = models.ForeignKey(User, SET_NULL, null=True, blank=True, related_name='updated_%(model_name)ss')

    class Meta:
        abstract = True
        ordering = ('id',)


class Server(BaseModel):
    INFO = 'INFO'
    DEBUG = 'DEBUG'

    LOG_TYPE = (
        (INFO, 'Info'),
        (DEBUG, 'Debug'),
    )

    name = models.CharField(max_length=255, validators=[MinLengthValidator(4)], default='Roomio Server')
    app_host = models.CharField(max_length=255, default='0.0.0.0')
    app_port = models.IntegerField(default=1880, verbose_name='Rest server port')
    timeout = models.IntegerField(default=2)
    logging = models.CharField(max_length=255, choices=LOG_TYPE, default=INFO)
    check_modbus_interval = models.IntegerField(default=60)
    check_modbus_interval_after_failed_attempt = models.IntegerField(default=120)
    objects = ServerQuerySet.as_manager()

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'main_server'
        verbose_name = 'Server'
        verbose_name_plural = 'Servers'


class ModbusServer(BaseModel):
    ONE = '1'
    DEVICE_TEMPLATE_ID = (
        (ONE, '1'),
    )
    device_name = models.CharField(max_length=255, default='HRC room 163')
    hrc_ip = models.CharField(max_length=255, default='0.0.0.0')
    hrc_port = models.IntegerField(default=502)
    unit_id = models.IntegerField(default=255)
    device_template_id = models.CharField(max_length=255, choices=DEVICE_TEMPLATE_ID, default=ONE)
    room = models.IntegerField(default=1)
    device_active = models.BooleanField(default=True)
    server = models.ForeignKey(Server, CASCADE, 'modbus_servers')
    objects = ModbusServerQuerySet.as_manager()

    def __str__(self):
        return self.device_name

    class Meta:
        db_table = 'main_modbus_server'
        verbose_name = 'ModbusServer'
        verbose_name_plural = 'ModbusServers'


class Location(BaseModel):
    device_location = models.CharField(max_length=255, default='master')
    modbus_server = models.ForeignKey(ModbusServer, CASCADE, 'locations')
    objects = LocationQuerySet.as_manager()

    def __str__(self):
        return self.device_location

    class Meta:
        db_table = 'main_location'
        verbose_name = 'Location'
        verbose_name_plural = 'Locations'


class Scene(BaseModel):
    name = models.CharField(max_length=255, default='welcome')
    modbus_server = models.ForeignKey(ModbusServer, CASCADE, 'scenes')
    objects = SceneQuerySet.as_manager()

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'main_scene'
        verbose_name = 'Scene'
        verbose_name_plural = 'Scenes'


class Tag(BaseModel):
    modbus_id = models.IntegerField(default=123)
    location = models.ForeignKey(Location, CASCADE, 'tags')
    objects = TagQuerySet.as_manager()

    def __str__(self):
        return str(self.modbus_id)

    class Meta:
        db_table = 'main_tag'
        verbose_name = 'Tag'
        verbose_name_plural = 'Tags'


class SceneTag(BaseModel):
    modbus_id = models.IntegerField(default=123)
    value = models.CharField(default=1)
    scene = models.OneToOneField(Scene, CASCADE, unique=True, related_name='scene_tag')
    objects = SceneTagQuerySet.as_manager()

    def __str__(self):
        return str(self.modbus_id)

    class Meta:
        db_table = 'main_scene_tag'
        verbose_name = 'Scene Tag'
        verbose_name_plural = 'Scene Tags'
