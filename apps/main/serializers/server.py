from rest_framework import serializers
from main.models import Server
from main.serializers.modbus_server import ModbusServerSerializer


class ServerSerializer(serializers.ModelSerializer):
    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['modbus_servers'] = ModbusServerSerializer(instance.modbus_servers, many=True).data
        return data

    class Meta:
        model = Server
        fields = (
            'id',
            'name',
            'app_host',
            'app_port',
            'timeout',
            'logging',
            'check_modbus_interval',
            'check_modbus_interval_after_failed_attempt',
        )
        read_only_fields = ('modbus_servers',)
