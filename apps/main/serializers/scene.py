from rest_framework import serializers
from main.models import Scene
from main.serializers.scene_tag import SceneTagSerializer


class SceneSerializer(serializers.ModelSerializer):
    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['tags_and_value'] = SceneTagSerializer(instance.scene_tag).data if hasattr(instance, 'scene_tag') else None
        return data

    class Meta:
        model = Scene
        fields = ('id', 'name', 'modbus_server')
