from rest_framework import serializers
from main.models import Tag


class TagSerializer(serializers.ModelSerializer):
    def to_representation(self, instance):
        data = super().to_representation(instance)
        return data

    class Meta:
        model = Tag
        fields = ('id', 'modbus_id', 'location')
