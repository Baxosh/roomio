from rest_framework import serializers
from main.models import SceneTag


class SceneTagSerializer(serializers.ModelSerializer):
    class Meta:
        model = SceneTag
        fields = (
            'id',
            'modbus_id',
            'value',
            'scene',
        )
