from rest_framework import serializers

from main.models import Location
from main.serializers.tag import TagSerializer


class LocationSerializer(serializers.ModelSerializer):
    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['tags'] = TagSerializer(instance.tags, many=True).data
        return data

    class Meta:
        model = Location
        fields = ('id', 'device_location', 'modbus_server')
