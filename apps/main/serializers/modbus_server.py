from rest_framework import serializers
from main.models import ModbusServer
from main.serializers.location import LocationSerializer
from main.serializers.scene import SceneSerializer


class ModbusServerSerializer(serializers.ModelSerializer):
    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['device_locations'] = LocationSerializer(instance.locations, many=True).data
        data['scenes'] = SceneSerializer(instance.scenes, many=True).data
        return data

    class Meta:
        model = ModbusServer
        fields = (
            'id',
            'device_name',
            'hrc_ip',
            'hrc_port',
            'unit_id',
            'device_template_id',
            'room',
            'device_active',
            'server',
        )
