from django.urls import path

from main.views.home import Home
from main.views.location import LocationListView, LocationDetailView
from main.views.modbus_server import ModbusServerListView, ModbusServerDetailView
from main.views.scene import SceneListView, SceneDetailView
from main.views.scene_tag import SceneTagListView
from main.views.server import ServerListView, ServerDetailView
from main.views.tag import TagListView, TagDetailView

urlpatterns = [
    path('home/', Home.as_view(), name='home-page'),

    path('server/', ServerListView.as_view(), name='server-list-view'),
    path('server/<int:pk>', ServerDetailView.as_view(), name='server-detail-view'),

    path('modbus-server/', ModbusServerListView.as_view(), name='modbus-server-list-view'),
    path('modbus-server/<int:pk>', ModbusServerDetailView.as_view(), name='modbus-server-detail-view'),

    path('location/', LocationListView.as_view(), name='location-list-view'),
    path('location/<int:pk>', LocationDetailView.as_view(), name='location-detail-view'),

    path('scene/', SceneListView.as_view(), name='scene-list-view'),
    path('scene/<int:pk>', SceneDetailView.as_view(), name='scene-detail-view'),

    path('tag/', TagListView.as_view(), name='tag-list-view'),
    path('tag/<int:pk>', TagDetailView.as_view(), name='tag-detail-view'),

    path('scene-tag/', SceneTagListView.as_view(), name='scene-tag-list-view'),
]
