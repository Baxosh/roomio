from django.db.models import QuerySet


class ServerQuerySet(QuerySet):
    def list(self):
        query = self.prefetch_related('modbus_servers')
        query = query.prefetch_related('modbus_servers__locations')
        query = query.prefetch_related('modbus_servers__locations__tags')
        query = query.prefetch_related('modbus_servers__scenes')
        return query.first()
